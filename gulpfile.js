const gulp = require("gulp")
const embedTemplates = require("gulp-angular-embed-templates")
const sass = require("gulp-sass")
const concat = require("gulp-concat")
const cssmin = require("gulp-cssmin")
const rename = require("gulp-rename")
const postcss = require("gulp-postcss")
const webserver = require("gulp-webserver")
const autoprefixer = require("autoprefixer")
const babel = require("gulp-babel")
const portfinder = require("portfinder")

const beeper = require("beeper")
const log = require("fancy-log")
const chalk = require("chalk")

const filePaths = {
	source: {
		html: ["./"],
		htmlComponents: ["src/html/**/*.html"],
		javascript: ["src/js/**/*.js"],
		scss: ["src/sass/**/*.scss"]
	},
	destination: {
		javascript: "dist/assets/js",
		scss: "dist/assets/css"
	}
}

gulp.task("sass", () => {
	return gulp
		.src(filePaths.source.scss)
		.pipe(
			sass().on("error", function(error) {
				beeper(2)
				log.error(chalk.bgRed(`SASS Error!`))
				log.error("Plugin:", chalk.cyan(error.plugin))
				log.error(chalk.red(error.message))

				this.emit("end")
			})
		)
		.pipe(
			postcss([
				autoprefixer({
					remove: false,
					browsers: ["IE >= 9", "Chrome >= 37", "Safari >= 7"]
				})
			])
		)
		.pipe(gulp.dest(filePaths.destination.scss))
		.pipe(cssmin())
		.pipe(
			rename(path => {
				path.extname = ".min.css"
			})
		)
		.pipe(gulp.dest(filePaths.destination.scss))
})

gulp.task("webserver", async () => {
	const port = await portfinder.getPortPromise()

	// need to manually increment the basePort since the first port isn't in use by anything yet.
	portfinder.basePort = port + 1

	const liveReloadPort = await portfinder.getPortPromise()

	return gulp.src(filePaths.source.html).pipe(
		webserver({
			port,
			host: "0.0.0.0",
			open: true,
			fallback: "index.html",
			livereload: {
				enable: true,
				port: liveReloadPort
			},
			directoryListing: {
				enable: false,
				path: filePaths.source.html
			}
		})
	)
})

gulp.task("javascript", () => {
	return gulp
		.src(filePaths.source.javascript)
		.pipe(
			babel({
				presets: [
					[
						"env",
						{
							useBuiltIns: true
						}
					]
				],
				plugins: [
					["transform-object-rest-spread", { useBuiltIns: true }],
					"syntax-object-rest-spread",
					"transform-regenerator",
					"babel-plugin-syntax-async-functions"
				]
			}).on("error", function(error) {
				beeper(2)
				log.error(chalk.bgRed(`Javascript Error! (${error.name})`))
				log.error(chalk.red(error.message)), log.error(error.loc)
				log.error("Plugin:", chalk.cyan(error.plugin))
				log.error(error.stack)

				this.emit("end")
			})
		)
		.pipe(concat("bundle.js"))
		.pipe(embedTemplates())
		.pipe(gulp.dest(filePaths.destination.javascript))
})

gulp.task("watch", ["webserver"], () => {
	gulp.watch(filePaths.source.scss, ["sass"])
	gulp.watch(filePaths.source.javascript, ["javascript"])
	gulp.watch(filePaths.source.htmlComponents, ["javascript"])
})

gulp.task("build", ["sass", "javascript"])
