# Template Starter - CLEAN PALETTE VERSION

This is a starter framework for creating templates for template-driven apps that can be served via the TDS.

## Getting Started

* **NOTE:** This template relies on @iw/ prefixed npm packages which are located on  the IW npm registry currently located at 10.16.4.209.
Use the [Getting Started](https://industryweapon.atlassian.net/wiki/spaces/APP/pages/3342352/Getting+Started) docs to set up your local npm environment. **This step is required before any other steps.**

* `git clone <link-to-this-repo>`

* `npm install` - this will install any dependencies

* Use `gulp watch` to watch css & js files.

* When finished, `npm run package` will create a tar file that can be uploaded to the TDS

## Development
* Quick Files to get started: main.tpl.html, styles.scss, template.controller.js, and manifest.json
* By default there is no `img` folder in dist/assets, so if you place images and logos they will be  `dist/assets/img/place-image-here.png`


### Working With Data, and the iwDataService service.

iwDataService is an Angular service that exposes a single method for retrieving data.

Example:

````
iwDataService.get({ appcode: 'eventsapp', service: 'ical' }).then(data => doSomethingWith(data))
````

you can also pass in live data if you have a valid API set up

Example:

````
iwDataService.get({ service: 'live', url: 'http://link-to-api.com/'})then(data => doSomethingWith(data))
````

For more on iwDataService, please see the [template-engine documentation](https://bitbucket.org/appsintegrations/iw-template-engine).