"use strict";

/*
Welcome to the Template Controler!
Controller for tempalte creation.
Here you will be able to form and manipulate data and other logic
Please remove from where it says remove in the comments.
*/

function TemplateController(iwDataService, CCHDAPI, $interval, templatePath) {
	var _this = this;

	/* This is the template path for things like importing images */
	this.templatePath = templatePath;

	/* 	
    Pagiation Controls, these variables may be changed
    depeding on the template design. Also when developing,
    you may set 'isOn' to false to stop pagination.
    */
	this.pagination = {
		perPage: 1,
		currentPage: 1,
		slideDuration: 2,
		isOn: false
		/******************* Remove from HERE **********************/

	};this.filePaths = {
		manifest: "dist/manifest.json",
		html: "dist/html/main.tpl.html",
		styles: "src/sass/styles.scss",
		stylesFolder: "src/sass/",
		templateController: "src/js/template.controller.js",
		logo: "dist/assets/img/logo.png",
		images: "dist/assets/img",
		jsFolder: "src/js/",
		colors: "src/sass/_colors.scss"
	};
	this.quickNote = "*Some quick notes on getting you started, Happy Coding!!!";

	/******************* to HERE **********************/

	/*
    ==============================================================================================
         						Using iwDataService to get data 
    */

	iwDataService.get({ appcode: "eventsapp", service: "ical" }).then(function (server) {
		Object.keys(server).forEach(function (key) {
			_this[key] = server[key];
		});

		/******************* Remove from HERE **********************/

		/*INTRODUCTION FROM THE CONSOLE PLEASE REMOVE*/
		console.log("==========================================");
		console.log("Welcome the the tpl controller.... Data section ");
		console.log(_this);

		console.log("==============template.controller.js===========");
		/*
        Insert your code here, For example I created this.data1
        which is just a simple array of objects
        Now we are going to make them paginate in main.tpl.html
        */
		_this.data1 = [{ slide: "January" }, { slide: "Febuary" }, { slide: "March" }, { slide: "April" }, { slide: "May" }, { slide: "June" }, { slide: "July" }, { slide: "August" }, { slide: "September" }, { slide: "October" }, { slide: "November" }, { slide: "December" }];

		/******************* to HERE **********************/

		/*
        ===========================================================================================
        							Pagination and Time
        */

		/* 
        Pagination Logic
  * *NOTE* Remember to change this.data1 to this.data in this.pagination.totlalPages
  *  because that will be the data
  * you are trying to paginate most likely.
         */

		//this.pagination.totalPages = Math.max(Math.ceil(this.data.length / this.pagination.perPage), 1)
		_this.pagination.totalPages = Math.max(Math.ceil(_this.data1.length / _this.pagination.perPage), 1);

		CCHDAPI.apis.resetSlideTimeout(_this.pagination.slideDuration * _this.pagination.totalPages);

		if (_this.pagination.isOn == true) {
			if (_this.pagination.totalPages > 1) {
				var pageChanger = $interval(function () {
					_this.pagination.currentPage++;

					if (_this.pagination.currentPage > _this.pagination.totalPages) {
						_this.pagination.currentPage = 1;
					}
				}, _this.pagination.slideDuration * 1000);
			}
		}
	});

	/*
    The code below is for TIME
    We combined moment with our CCHDAPI to present time
    the format may be changed from here.
    */
	var updateTime = function updateTime() {
		var date = moment(CCHDAPI.time.getLocalTime());

		_this.currentDate = date.format("dddd, MMMM, Do, YYYY");
		_this.currentTime = date.format("h:mm");
		_this.currentAmPm = date.format("A");
	};

	updateTime();
	var timeInterval = $interval(updateTime, 30000);
}

var app = angular.module("app");
app.controller("TemplateController", ["iwDataService", "CCHDAPI", "$interval", "templatePath", TemplateController]);